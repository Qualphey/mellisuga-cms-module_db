
module.exports = class {
  constructor(cmbird, table) {
    this.table = table;

    let app = cmbird.app;


    app.post("/modules.io", cmbird.auth.orize, async function(req, res) {
      try {
        let data = JSON.parse(req.body.data);
        switch(data.command) {
            case "publish":
            let module = data.module;
            try {
              if (!module.type) throw "NO MODULE TYPE!";
              if (!module.name) throw "NO MODULE NAME!";
              if (!module.description) throw "NO MODULE DESCRIPTION!";
              if (!module.version) throw "NO MODULE VERSION!";
              if (!module.repository) throw "NO MODULE REPOSITORY!";

              const name_exists = await table.select(["version", "created_by"], "name = $1 AND type = $2", [module.name, module.type]);
              if (name_exists.length > 0) {
                console.log(module.version, name_exists[0].version);
                if (versionCompare(module.version, name_exists[0].version) > 0) {
                  if (name_exists[0].created_by === req.session_data.id) {
                    await table.update({
                      type: module.type,
                      description: module.description,
                      version: module.version,
                      repository: module.repository
                    }, "name = $1 AND created_by = $2", [module.name, req.session_data.id]);

                    res.send({
                      success: module.name+"@"+module.version+" published!"
                    });
                  } else {
                    throw "UNAUTHORIZED!";
                  }
                } else {
                  throw "MODULE VERSION MUST BE GREATER TO RE-PUBLISH THIS MODULE";
                }
              } else {
                await table.insert({
                  created_by: req.session_data.id,
                  type: module.type,
                  name: module.name,
                  description: module.description,
                  version: module.version,
                  repository: module.repository
                });

                res.send({
                  success: module.name+"@"+module.version+" published!"
                });
              }
            } catch (e) {
              res.send(e);
            }

            break;
          default:

        };
      } catch (e) {
        console.error(e.stack);
      }
    });

    app.get("/modules.io", async function(req, res) {
      try {
        let data = JSON.parse(req.query.data);
        switch (data.command) {
          case "install":
            let repo_found = await table.select(["repository", "version"], "type = $1 AND name = $2", [
              data.type, data.name
            ]);
            if (repo_found.length > 0) {
              res.send(JSON.stringify({
                success: {
                  repo: repo_found[0].repository,
                  version: repo_found[0].version
                }
              }));
            } else {
              res.send("MODULE NOT FOUND!");
            }
            break;
          case "all":
            let cms_list = await table.select(
              ["name", "type", "version"],
              "type = $1",
              ["cms"]
            );
            let web_list = await table.select(
              ["name", "type", "version"],
              "type = $1",
              ["web"]
            ) || [];
            
            res.json({
              cms: cms_list,
              web: web_list
            });
            break;
          case "search":
            let search_result = await table.select(["name", "type", "version"], "name ~ $1", [data.search_val]);
            res.json(search_result);
          default:
        }
        console.log(req.query);
      } catch (e) {
        console.log(e.stack);
      }
    });
  }

  static async init(cmbird) {
    var table = await cmbird.aura.table('modules', {
      columns: {
        id: 'uuid',
        created_by: 'UUID',
        type: 'varchar(4)',
        name: 'varchar(256)',
        description: 'varchar(1024)',
        version: 'varchar(32)',
        repository: 'varchar(512)'
      }
    });
    return new module.exports(cmbird, table);
  }
}

function versionCompare(v1, v2, options) {
    var lexicographical = options && options.lexicographical,
        zeroExtend = options && options.zeroExtend,
        v1parts = v1.split('.'),
        v2parts = v2.split('.');

    function isValidPart(x) {
        return (lexicographical ? /^\d+[A-Za-z]*$/ : /^\d+$/).test(x);
    }

    if (!v1parts.every(isValidPart) || !v2parts.every(isValidPart)) {
        return NaN;
    }

    if (zeroExtend) {
        while (v1parts.length < v2parts.length) v1parts.push("0");
        while (v2parts.length < v1parts.length) v2parts.push("0");
    }

    if (!lexicographical) {
        v1parts = v1parts.map(Number);
        v2parts = v2parts.map(Number);
    }

    for (var i = 0; i < v1parts.length; ++i) {
        if (v2parts.length == i) {
            return 1;
        }

        if (v1parts[i] == v2parts[i]) {
            continue;
        }
        else if (v1parts[i] > v2parts[i]) {
            return 1;
        }
        else {
            return -1;
        }
    }

    if (v1parts.length != v2parts.length) {
        return -1;
    }

    return 0;
}


